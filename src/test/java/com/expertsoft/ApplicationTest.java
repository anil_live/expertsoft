package com.expertsoft;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.expertsoft.dao.UserDao;
import com.expertsoft.model.UserDetails;
import com.expertsoft.service.impl.UserServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTest {

	@Autowired
	private UserServiceImpl userServiceimpl;

	@MockBean
	private UserDao dao;

	@Test
	public void getUserDetailsTest() {
		when(dao.getUserDetails())
				.thenReturn(Stream
						.of(new UserDetails(376, "Danile", "lastName", "USA", "password"),
								new UserDetails(376, "Danile", "lastName", "USA", "password"))
						.collect(Collectors.toList()));
		assertEquals(2, userServiceimpl.getUserDetails().size());
	}

	@Test
	public void updateUserDetailTest() {
		UserDetails userDetails = new UserDetails(376, "Danile", "lastName", "USA", "password");
		when(dao.updateUserDetail(userDetails)).thenReturn(true);
	}

}
