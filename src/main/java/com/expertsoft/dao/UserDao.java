package com.expertsoft.dao;

import java.util.List;

import com.expertsoft.model.UserDetails;

public interface UserDao {

	List<UserDetails> getUserDetails();

	boolean updateUserDetail(UserDetails details);

	public void addUser(UserDetails details);

	void deleteUser(UserDetails details);

}
