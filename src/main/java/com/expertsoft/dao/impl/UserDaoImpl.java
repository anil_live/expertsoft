package com.expertsoft.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.expertsoft.dao.UserDao;
import com.expertsoft.model.UserDetails;

@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
	private SessionFactory sessionFactory;

	public List<UserDetails> getUserDetails() {
		Criteria criteria = sessionFactory.openSession().createCriteria(UserDetails.class);
		return criteria.list();
	}

	public boolean updateUserDetail(UserDetails details) {
		Session session = sessionFactory.openSession();
		session.update(details);
		System.out.println("anil");
		return true;
	}

	public void addUser(UserDetails details) {
		Session session = sessionFactory.openSession();
		session.save(details);
	}

	public void deleteUser(UserDetails details) {
		Session session = sessionFactory.openSession();
		session.delete(details);
	}
}
