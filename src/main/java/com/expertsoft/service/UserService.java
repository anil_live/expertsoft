/**
 * 
 */
package com.expertsoft.service;

import java.util.List;

import com.expertsoft.model.UserDetails;

public interface UserService {

	List<UserDetails> getUserDetails();

	boolean updateUserDetail(UserDetails details);

	void addUser(UserDetails details);

	void deleteUser(UserDetails details);

}
