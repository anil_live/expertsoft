/**
 * 
 */
package com.expertsoft.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.expertsoft.dao.UserDao;
import com.expertsoft.model.UserDetails;
import com.expertsoft.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	public List<UserDetails> getUserDetails() {
		return userDao.getUserDetails();

	}

	public boolean updateUserDetail(UserDetails details) {
		return userDao.updateUserDetail(details);

	}

	public void addUser(UserDetails details) {
		userDao.addUser(details);
	}

	public void deleteUser(UserDetails details) {
		userDao.deleteUser(details);
	}
}
