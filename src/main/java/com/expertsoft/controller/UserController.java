package com.expertsoft.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.expertsoft.model.UserDetails;
import com.expertsoft.service.UserService;

@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
	public List<UserDetails> userList() {
		List<UserDetails> userDetails = userService.getUserDetails();
		return userDetails;
	}

	@RequestMapping(value = "/getUserByName/{name}", method = RequestMethod.GET)
	public UserDetails getUserByName(@PathVariable("name") String name) {
		List<UserDetails> userDetails = userService.getUserDetails();
		UserDetails details = null;
		for (UserDetails userDetails2 : userDetails) {
			if (userDetails2.getFirstName().equalsIgnoreCase(name)) {
				details = userDetails2;
				break;
			}
		}
		return details;
	}

	@RequestMapping(value = "/getUserByName/{name}", method = RequestMethod.PUT)
	public ResponseEntity<Boolean> updateUserByName(@PathVariable("name") String name,
			@RequestBody UserDetails details) {
		boolean isupdated = false;
		List<UserDetails> userDetails = userService.getUserDetails();
		HttpHeaders headers = new HttpHeaders();
		headers.add("key1", "value1");
		headers.add("key2", "value2");
		for (UserDetails userDetails2 : userDetails) {
			if (userDetails2.getFirstName().equalsIgnoreCase(name)) {
				userService.updateUserDetail(details);
				isupdated = true;
				break;

			} else {
				isupdated = false;
			}

		}
		return new ResponseEntity<Boolean>(isupdated, headers, HttpStatus.NOT_FOUND);

	}
	

	@RequestMapping(value = "/addUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> addUser(@RequestBody UserDetails details) {
		userService.addUser(details);
		return new ResponseEntity<Boolean>(true, HttpStatus.CREATED);

	}

	@RequestMapping(value = "/deleteUserByName/{firstName}", method = RequestMethod.DELETE)
	public void deleteUserByName(@PathVariable("firstName") String firstName) {
		List<UserDetails> userDetails = userService.getUserDetails();
		for (UserDetails userDetails2 : userDetails) {
			if (userDetails2.getFirstName().equalsIgnoreCase(firstName)) {
				userService.deleteUser(userDetails2);
				break;

			}
		}
	}
}
